'use strict';
var https = require("https");
var Discord = require('discord.io');
var mongoose = require('mongoose');
var minimist = require('minimist');
var ObjectId = require('mongoose').Types.ObjectId;

var hotsMap = new Object();
hotsMap["dima"] = "1878262";
hotsMap["tyler"] = "2207760";
hotsMap["brian"] = "2207761"
hotsMap["rj"] = "2191963";

var triviaCategories = {    
    '10' : 'Entertainment: Books',
    '11' : 'Entertainment: Film',
    '12' : 'Entertainment: Music',
    '13' : 'Entertainment: Musicals & Theatres',
    '14' : 'Entertainment: Television',
    '15' : 'Entertainment: Video Games',
    '16' : 'Entertainment: Board Games',
    '17' : 'Science & Nature',
    '18' : 'Science: Computers',
    '19' : 'Science: Mathematics',
    '20' : 'Mythology',
    '21' : 'Sports',
    '22' : 'Geography',
    '23' : 'History',
    '24' : 'Politics',
    '25' : 'Art',
    '26' : 'Celebrities',
    '27' : 'Animals',
    '28' : 'Vehicles',
    '29' : 'Entertainment: Comics',
    '30' : 'Science: Gadgets',
    '31': 'Entertainment: Japanese Anime & Manga',
    '32': 'Entertainment: Cartoon & Animations'
}



var users = {
    "107120826809098240": "Dima",
    "294507725570703360": "Matt",
    "108569854625210368": "Steve",
    "107118576208445440": "Brian",
    "107119078518312960": "Andy",
    "107121099690496000": "Tyler",
    "107119910269771776": "RJ"
}

var userToId = {
    "Dima" : "107120826809098240",
    "Matt" : "294507725570703360",
    "Steve": "108569854625210368",
    "Brian": "107118576208445440",
    "Andy": "107119078518312960",
    "Tyler": "107121099690496000",
    "RJ": "107119910269771776"
}

var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var PostSchema = new Schema({    
    command : String, 
    message : String
});


var TriviaResultSchema = new Schema({
    category: String,
    correct: Number,
    total: Number
})

var UserSchema = new Schema({
    triviaResults: [TriviaResultSchema],
    _id: Number
});

var GlobalSchema = new Schema({
    posts: [PostSchema]    
});

var LinkSchema = new Schema({
    url: String,
    firstPosted: { type: Date, default: Date.now },
    ogPoster: String
})

console.log("hi")
mongoose.connect('mongodb://localhost/test');

console.log("hi bye")

var Post = mongoose.model('PostSchema', PostSchema);
var Global = mongoose.model('Global', GlobalSchema);
var User = mongoose.model('User', UserSchema);
var TriviaResult = mongoose.model('TriviaResultSchema', TriviaResultSchema);
var Link = mongoose.model('LinkSchema', LinkSchema);

var currentTrivia = null;
var currentTimeout = null;

var shameGifs = [
    "http://gph.is/1PQeBVs",
    "http://gph.is/28KV6Qh",
    "http://gph.is/2cI3chF",
    "http://gph.is/2k5YDm1",
    "https://giphy.com/gifs/shame-for-box-of-eP1fobjusSbu"
]

var bot = new Discord.Client({
    token: "Mjk3MDA0NTUwNzY0NzU2OTkz.DrO29Q.7ki-SiAUCX3zspBsE8WLOgw0z7U",
    autorun: true
})

bot.on('ready', function () {
    console.log('Logged in as %s - %s\n', bot.username, bot.id);
})

bot.on('message', function (user, userId, channelId, message, event) {


    let messageId = event.d.id;
    if (userId != bot.id) {
        var regex = "([a-zA-Z0-9]+://)?([a-zA-Z0-9_]+:[a-zA-Z0-9_]+@)?([a-zA-Z0-9.-]+\\.[A-Za-z]{2,4})(:[0-9]+)?(/.*)?";
        if (new RegExp(regex).test(message) && message.indexOf("giphy") == -1 && message.indexOf("gif") == -1) {
            var urlRegex = /\bhttps?:\/\/\S+/ig;
            var url = message.match(regex)[0];
            console.log(url);
            Link.find({}, function (err, docs) {
                var notRepost = true;
                var userName = users[userId];
                for (var i = 0; i < docs.length; i++) {
                    if (docs[i].url == url) {
                        console.log("repost");
                        notRepost = false;
                        var repostedDoc = docs[i];
                        bot.sendMessage({
                            to: channelId,
                            message: "Shame on " + userName + "!! " + repostedDoc.ogPoster + " Posted this on " + repostedDoc.firstPosted + shameGifs[Math.floor(Math.random() * shameGifs.length)]
                        });
                    }
                }
                if (notRepost) {
                    console.log("not repost")
                    var newLink = new Link();
                    newLink.url = url;
                    newLink.ogPoster = users[userId];
                    Link.create(newLink, function (err) {
                        console.log("link created");
                    });
                }
            })
        }

        if (message == "!colbert") {
            https.get(`https://www.googleapis.com/youtube/v3/search?part=id&q=colbert&type=video&key=AIzaSyDPT9hQhM-ZZ4mppyfLRLs0N8WOv9QmyXg&order=date&channelId=UCMtFAi84ehTSYSE9XoHefig`, function (res) {
                let str = '';
                res.on('data', function (data) {
                    str += data;
                });
                res.setEncoding('utf8');

                res.on('end', function () {
                    console.log(str);
                    let data = JSON.parse(str);
                    console.log(data);
                    let videoId = data.items[0].id.videoId;
                    bot.sendMessage({
                        to: channelId,
                        message: "https://www.youtube.com/watch?v=" + videoId,
                    });
                });
            });
        } else if (message == "!shitpost-vid") {
            https.get(`https://www.googleapis.com/youtube/v3/playlistItems?&key=AIzaSyDPT9hQhM-ZZ4mppyfLRLs0N8WOv9QmyXg&playlistId=PLWeKqk6tXIbMhruHS00q3TiOCVwmRiwdL&part=contentDetails&maxResults=50`, function (res) {
                let str = '';
                res.on('data', function (data) {
                    str += data;
                });
                res.setEncoding('utf8');

                res.on('end', function () {
                    let data = JSON.parse(str);
                    let maxLength = data.items.length;
                    console.log(data);
                    let videoId = data.items[Math.floor(Math.random() * maxLength) + 1].contentDetails.videoId;
                    bot.sendMessage({
                        to: channelId,
                        message: "https://www.youtube.com/watch?v=" + videoId,
                    });
                });
            });
        } else if (message == "!shitpost") {
            let offset = Math.floor(Math.random() * 1) + 1
            https.get("https://api.giphy.com/v1/gifs/search?q=shitpost&api_key=dc6zaTOxFJmzC&limit=100&offset=" + offset, function (res) {
                let str = '';
                res.on('data', function (data) {
                    str += data;
                });
                res.setEncoding('utf8');

                res.on('end', function () {
                    let data = JSON.parse(str).data;
                    let maxLength = data.length;
                    let image = data[0];
                    console.log(image["images"]["fixed_height"]);
                    let imageUrl = data[Math.floor(Math.random() * maxLength) + 1]["images"]["downsized"]["url"];
                    postMessage(channelId, imageUrl, user, messageId);
                });
            });
        } else if (message.startsWith("!meme")) {
            let memeMessage = message.replace("!meme ", "");
            let memeArray = memeMessage.split("/");
            let template = memeArray[0];

            let topMessage = "-";
            if (memeArray.length > 1) {
                topMessage = memeArray[1];
                topMessage = topMessage.replace("?", "~q");

                topMessage = topMessage.split(' ').join('-');
            }

            let bottomMessage = "-";
            if (memeArray.length > 1) {
                bottomMessage = memeArray[2];
                bottomMessage = bottomMessage.replace("?", "~q");
                bottomMessage = bottomMessage.split(' ').join('-');
            }

            postMessage(channelId, "https://memegen.link/" + template + "/" + topMessage + "/" + bottomMessage + ".jpg", user, messageId);
        } else if (message.startsWith("!hots")) {
            console.log("here");
            let username = message.replace("!hots ", "");
            let id = hotsMap[username];
            console.log(username);
            console.log(id);
            https.get("https://api.hotslogs.com/Public/Players/" + id, function (res) {
                let str = '';
                res.on('data', function (data) {
                    str += data;
                });
                res.setEncoding('utf8');
                res.on('end', function () {
                    let data = JSON.parse(str);
                    let rankings = data.LeaderboardRankings;
                    let response = "Quick Match : " + rankings[0].CurrentMMR + "\n" + "Hero League : " + rankings[1].CurrentMMR + "\n" + "Team League : " + rankings[2].CurrentMMR;
                    bot.sendMessage({
                        to: channelId,
                        message: response,
                    });
                });
            });
        } else if (message.startsWith("!addGlobalCommand")) {
            console.log(message);
            message = message.replace("!addGlobalCommand ", "");

            let messageArray = message.split("/");
            let command = messageArray[0];
            let text = message.replace(command + "/", "");
            console.log(text);

            Global.find({}, function (err, docs) {
                console.log(docs);
                let commandExists = false;
                let globalDoc = docs[0];
                for (let i = 0; i < globalDoc.posts.length; i++) {
                    let post = globalDoc.posts[i];
                    if (post.command == command) {
                        commandExists = true;
                    }
                }
                if (!commandExists) {
                    var post = new Post();
                    post.command = command;
                    post.message = text;
                    globalDoc.posts.push(post);
                    globalDoc.save(function (err) {
                        if (!err) {
                            console.log("Success adding " + command);
                        }
                    });
                } else {
                    console.log("Command Already Exists")
                }
                console.log(globalDoc);
            });
        } else if (message.startsWith("!removeGlobalCommand")) {
            let command = message.replace("!removeGlobalCommand ", "");
            Global.find({}, function (err, docs) {
                let globalDoc = docs[0];
                for (let i = 0; i < globalDoc.posts.length; i++) {
                    let post = globalDoc.posts[i];
                    if (post.command == command) {
                        globalDoc.posts[i].remove();
                        globalDoc.save(function (err) {
                            if (!err) {
                                bot.sendMessage({
                                    to: channelId,
                                    message: "Command Deleted"
                                });
                            }
                        });
                    }
                }
            });
        } else if (message.startsWith("!price")) {
            let symbol = message.replace("!price ", "");
            let url = "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=" + symbol + "&interval=1min&outputsize=compact&apikey=Q3RRU8XFBAHHJ9G2"
            console.log(url);
            https.get(url, function (res) {
                let str = '';
                res.on('data', function (data) {
                    str += data;
                });
                res.setEncoding('utf8');
                res.on('end', function () {
                    let data = JSON.parse(str);
                    console.log(data);
                    let timeObject = data["Time Series (1min)"]
                    if (timeObject != null) {
                        let currentPrice = timeObject[Object.keys(timeObject)[0]]["1. open"];
                        console.log(symbol + " : " + currentPrice);
                        bot.sendMessage({
                            to: channelId,
                            message: symbol + " : " + currentPrice,
                        });
                    } else {
                        bot.sendMessage({
                            to: channelId,
                            message: "Invalid Symbol"
                        });
                    }                   
                });
            });
        } else if (message == "?globalCommands") {
            Global.find({}, function (err, docs) {
                let globalDoc = docs[0];
                let response = "";
                globalDoc.posts.sort(function (a, b) {
                    console.log("sorting");
                    if (a.command < b.command) {
                        return -1;
                    } else if (a.command > b.command) {
                        return 1;
                    } else {
                        return 0;
                    }
                });
                console.log(globalDoc.posts);
                for (let i = 0; i < globalDoc.posts.length; i++) {
                    let post = globalDoc.posts[i];
                    response = response + "- " + post.command + "\n";
                }
                bot.sendMessage({
                    to: channelId,
                    message: response
                });
            });
        } else if (message == "?help") {
            let response =
                "!addGlobalCommand - Add a command that everyone can use follow the format `!addGlobalCommand {command}/{text}` (Don't prepend the ! to the command) \n\n" +
                "!removeGlobalCommand - Remove a previously created command \n\n" +
                "?globalCommands - Check what custom made commands are available \n\n" +
                "!meme - Generate a meme, Select a template from the list here https://memegen.link/api/templates/ then type in `!meme {template}/{top message}/{bottom message}` \n\n" +
                "!shitpost-vid - Post a random shitpost video \n\n" +
                "!shitpost - Post a random shitpost gif \n\n" +
                "!colbert - Post the latest colbert video \n\n" +
                "!trivia - use the flag `-d` to set the difficulty to ['easy', 'medium', 'hard'] and the flag '-c' to set the category. The categories can be seen using ?categories use `!answer` to answer the current trivia question \n\n" +
                "?myTriviaStats - see your trivia stats \n\n" +
                "?triviaStats - {name} - show other peoples stats";

            bot.sendMessage({
                to: channelId,
                message: response
            });
        } else if (message.startsWith("!trivia")) {
            var args = minimist(message.split(' '), {
                default: {
                    d: '',
                    c: '',
                },
                alias: {
                    'd': 'difficulty',
                    'c': 'category'
                }
            });
            https.get("https://opentdb.com/api.php?amount=1&category=" + args.category + "&difficulty=" + args.difficulty, function (res) {
                let str = '';
                res.on('data', function (data) {
                    str += data;
                });
                res.setEncoding('utf8');

                res.on('end', function () {
                    let data = JSON.parse(str);
                    let tQuestion = data.results[0];
                    let response = "Here is a " + tQuestion.difficulty + " question in the " + tQuestion.category + " category. The result is of type " + tQuestion.type + ". You have 1 minute to response! \n**" + tQuestion.question + "**";
                    let possibleAnswers = [];
                    if (tQuestion.type == "multiple") {
                        response = response + "\n";
                        console.log(tQuestion);
                        possibleAnswers.push(tQuestion.correct_answer);
                        for (let i = 0; i < tQuestion.incorrect_answers.length; i++) {
                            possibleAnswers.push(tQuestion.incorrect_answers[i]);
                        }
                        possibleAnswers = shuffle(possibleAnswers);
                        console.log(possibleAnswers.length);
                        for (let i = 0; i < possibleAnswers.length; i++) {
                            response = response + (i + 1) + " - " + possibleAnswers[i] + "\n";
                        }
                    }

                    bot.deleteMessage({
                        "channelID": channelId,
                        "messageID": messageId
                    });

                    bot.sendMessage({
                        to: channelId,
                        message: response
                    });
                    console.log(data);

                    bot.getAllUsers(function (error) {
                        console.log(error);
                    });

                    //let members = bot.servers["297004156588261376"].members; // Test server
                    let members = bot.servers["107123018932682752"].members;
                    let trivia = {
                        channelId: channelId,
                        question: tQuestion,
                        players: {},
                        possibleAnswers: possibleAnswers
                    }
                    Object.keys(members).forEach(function (key) {
                        let user = members[key];
                        if (user.status == 'online' && user.id != bot.id) {
                            trivia.players[user.id] = null;
                        }
                    });

                    currentTrivia = trivia;
                    if (currentTimeout != null) {
                        clearTimeout(currentTimeout);
                        currentTimeout = null;
                    }
                    currentTimeout = setTimeout(function () {
                        console.log(currentTrivia);
                        postTriviaResults();

                    }, 60000);
                    // console.log(currentTrivia);
                });
            });
        } else if (message.startsWith("!answer")) {
            let answer = message.replace("!answer ", "");
            if (currentTrivia == null) {
                console.log("no trivia question");
                bot.sendMessage({
                    to: channelId,
                    message: "You need to first start a Trivia question using !trivia"
                });
            } else {
                if (currentTrivia.question.type == "multiple" && answer.length == 1) {
                    answer = currentTrivia.possibleAnswers[parseInt(answer) - 1];
                }
                console.log(answer);
                console.log("Answering question for userid " + userId);
                currentTrivia.players[userId] = answer;
                let needResponse = false;
                Object.keys(currentTrivia.players).forEach(function (key) {
                    let response = currentTrivia.players[key];
                    console.log("Response for user " + key + " : " + response);
                    if (response == null) {
                        needResponse = true;
                    }
                });
                console.log("Need response " + needResponse);
                let correctAnswer = currentTrivia.question.correct_answer;
                if (!needResponse) {
                    postTriviaResults();
                }

                bot.deleteMessage({
                    "channelID": channelId,
                    "messageID": messageId
                });

                bot.sendMessage({
                    to: channelId,
                    message: users[userId] + " submitted his response"
                });
            }
        } else if (message == "?categories") {
            let response = "**The categories for trivia are** \n" + JSON.stringify(triviaCategories, null, 2);
            bot.sendMessage({
                to: channelId,
                message: response
            });
        } else if (message.startsWith("?myTriviaStats")) {
            var args = minimist(message.split(' '), {
                default: {
                    d: false,
                    c: '',
                },
                alias: {
                    'd': 'detailed',
                }
            });

            console.log(args.detailed);
            showTriviaResultsForUserId(userId, channelId, messageId, args.detailed);           
        } else if (message.startsWith("?triviaStats")) {
            var args = minimist(message.split(' '), {
                default: {
                    d: false,
                    u: '',
                },
                alias: {
                    'd': 'detailed',
                    'u': 'user'
                }
            });            
            let otherUserId = userToId[args.user];                        
            console.log(args.user);
            showTriviaResultsForUserId(otherUserId, channelId, messageId, args.detailed);
        } else if (message.startsWith("!")) {
            let command = message.replace("!", "");
            console.log("command", command);
            Global.find({}, function (err, docs) {                
                let globalDoc = docs[0];
                for (let i = 0; i < globalDoc.posts.length; i++) {
                    let post = globalDoc.posts[i];
                    console.log(post);
                    if (post.command == command) {
                        postMessage(channelId, post.message, user, messageId);
                    }
                }         
            });
        }
    }    
});

function showTriviaResultsForUserId(userId, channelId, messageId, detailedStats) {
    User.findById(userId, function (err, user) {
        console.log(err);
        let response = "**Stats for " + users[userId] + "**\n";
        let triviaResults = user.triviaResults;
        let correct = 0;
        let total = 0;
        for (let i = 0; i < triviaResults.length; i++) {
            let result = triviaResults[i];
            let percentage = result.correct == result.total ? 100 : Math.floor((result.correct / result.total) * 100);
            correct += result.correct;
            total += result.total;
            if (detailedStats) {
                response = response + result.category + " : " + result.correct + " / " + result.total + " - " + percentage + "%\n"
            }            
        }
        if (!detailedStats) {
            let percentage = Math.floor((correct / total) * 100)
            response = response + correct + " / " + total + " - " + percentage + "%";
        }
        bot.deleteMessage({
            "channelID": channelId,
            "messageID": messageId
        });
        bot.sendMessage({
            to: channelId,
            message: response
        });
    });
}

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

function saveTriviaResultForUser(userId, currentTrivia, isCorrect) {
    let user = null;
    User.findById(userId, function (err, user) {
       // console.log(err);
       // console.log(user);
        if (user == null) {           
            console.log("Creating new user");
            user = new User();
            user._id = userId;
            user.triviaResults = [];
            User.create(user, function (err) {
                console.log(err);
            });
        }
        let hasTriviaCategory = false;
        if (user.triviaResults == null) {
            user.triviaResults = [];
        }
        for (let i = 0; i < user.triviaResults.length; i++) {
            let triviaResult = user.triviaResults[i];
            if (triviaResult.category == currentTrivia.question.category) {
                console.log("Found category and updating");
                hasTriviaCategory = true;
                user.triviaResults[i].total++;
                if (isCorrect) {
                    user.triviaResults[i].correct++;
                }
                user.save();
            }
        }
        console.log(currentTrivia);
        if (!hasTriviaCategory) {
            console.log("Creating new category");
            let newResult = new TriviaResult();            
            newResult.category = currentTrivia.question.category;
            newResult.total = 1;
            newResult.correct = isCorrect ? 1 : 0;                 
            user.triviaResults.push(newResult);
            user.save(function(err) {
                console.log(err);
            });
        }
    });
}

function postTriviaResults() {
    let correctUsers = [];
    let wrongUsers = [];
    Object.keys(currentTrivia.players).forEach(function (key) {
        let userId = key;
        let response = currentTrivia.players[key];
        if (response != null) {
            if (response.toUpperCase() == currentTrivia.question.correct_answer.toUpperCase()) {
                saveTriviaResultForUser(userId, currentTrivia, true);
                correctUsers.push(users[userId]);
            } else {
                saveTriviaResultForUser(userId, currentTrivia, false);
                wrongUsers.push(users[userId]);
            }
        }        
    });

    let response = null;
    if (correctUsers.length == 0 && wrongUsers.length == 0) {
        response = "No one answered the trivia question :(";
    } else {
        response = "**The result are in! The answer to the question is** \n " + currentTrivia.question.correct_answer + "\n" +
            "**Smart People** - " + JSON.stringify(correctUsers, null, 2) + "\n" +
            "**Dumb People** - " + JSON.stringify(wrongUsers, null, 2);
    }    

    bot.sendMessage({
        to: currentTrivia.channelId,
        message: response
    });

    currentTrivia = null;
}

function postMessage(channelId, url, user, messageId) {
    url = "From - " + user + "\n" + url;
    bot.deleteMessage({
        "channelID" : channelId,
        "messageID" : messageId
    });
    bot.sendMessage({
        to: channelId,
        message: url
    });
}

function getKeyByValue(value, object) {
    for (var prop in object) {
        if (object.hasOwnProperty(prop)) {
            if (object[prop] === value)
                return prop;
        }
    }
}